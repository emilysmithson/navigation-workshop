import 'dart:developer';

import 'package:flutter/material.dart';

class Page1 extends StatefulWidget {
  final Color appBarColour;
  const Page1({
    Key? key,
    required this.appBarColour,
  }) : super(key: key);

  @override
  State<Page1> createState() => _Page1State();
}

class _Page1State extends State<Page1> {
  @override
  void initState() {
    log('initialising page 1');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    log('building page 1');
    return Scaffold(
      appBar: AppBar(
        backgroundColor: widget.appBarColour,
        title: const Text('Page 1'),
      ),
      body: Center(
        child: Column(
          children: [
            ElevatedButton(
              child: const Text('Pop'),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
